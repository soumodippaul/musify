package com.example.soumodippaul.newapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    CallbackManager callbackManager;
    TextView headTxt;
    TextView infoTxt;
    TextView infoTxtLogin;
    LoginButton facebookLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FACEBOOK
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);
        setFont();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null){
            //SHOW PROGRESS DIALOG
            ProgressDialog progressDlg=new ProgressDialog(this);
            progressDlg.setMessage("LOGGING IN TO MUSIFY");
            progressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDlg.setIndeterminate(true);
            progressDlg.show();
            //REMOVE THE VISIBILITY OF THE TWO BUTTONS
            infoTxtLogin.setVisibility(View.INVISIBLE);
            facebookLogin.setVisibility(View.INVISIBLE);
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    Log.d("SUCCESS", response.toString());
                    try {
                        setSharedPreferences();
                        Intent intent = new Intent(getApplicationContext(), musicPlayer.class);
                        intent.putExtra("NAME", object.getString("first_name") + " " + object.getString("last_name"));
                        intent.putExtra("EMAIL", object.getString("email"));
                        intent.putExtra("PROFILE_PIC", "https://graph.facebook.com/" + object.getString("id") + "/picture?width=200&height=200");
                        startActivity(intent);
                    } catch (JSONException e) {
                        Log.d("ERROR", "ERROR_PARSING_JSON");
                    }
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email"); // Parámetros que pedimos a facebook
            request.setParameters(parameters);
            request.executeAsync();
        }else {
            facebookLogin.setReadPermissions("email");
            facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    //REMOVE THE VISIBILITY OF THE TWO BUTTONS
                    infoTxtLogin.setVisibility(View.INVISIBLE);
                    facebookLogin.setVisibility(View.INVISIBLE);
                    //GET USER DATA FROM FACEBOOK
                    String accessToken = loginResult.getAccessToken().getToken();
                    Log.d("SUCCESS", loginResult.getAccessToken().getToken());
                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.d("SUCCESS", response.toString());
                            try {
                                setSharedPreferences();
                                Intent intent = new Intent(getApplicationContext(), musicPlayer.class);
                                intent.putExtra("NAME", object.getString("first_name") + " " + object.getString("last_name"));
                                intent.putExtra("EMAIL", object.getString("email"));
                                intent.putExtra("PROFILE_PIC", "https://graph.facebook.com/" + object.getString("id") + "/picture?width=200&height=200");
                                startActivity(intent);
                            } catch (JSONException e) {
                                Log.d("ERROR", "ERROR_PARSING_JSON");
                            }
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id, first_name, last_name, email"); // Parámetros que pedimos a facebook
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    Log.d("CANCEL", "USER CANCELLED THE LOGIN");
                }

                @Override
                public void onError(FacebookException error) {
                    Log.d("ERROR", "SOME ERROR HAPPENED");
                }
            });
            // GET HASH KEY
            try {
                PackageInfo info = getPackageManager().getPackageInfo("com.example.soumodippaul.newapp", PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.d("KEYHASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {

            } catch (NoSuchAlgorithmException e) {

            }
            //END OF HASK KEY
        }
    }

    private void setFont(){
        //CHANGE FONT
        Typeface josephSans=Typeface.createFromAsset(getAssets(),"fonts/josephSans.ttf");
        headTxt=(TextView)findViewById(R.id.headText);
        infoTxt=(TextView)findViewById(R.id.infoText);
        infoTxtLogin=(TextView)findViewById(R.id.infoTextLogin);
        facebookLogin = (LoginButton) findViewById(R.id.facebookLogin);
        headTxt.setTypeface(josephSans);
        infoTxt.setTypeface(josephSans);
        infoTxtLogin.setTypeface(josephSans);
        facebookLogin.setTypeface(josephSans);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setSharedPreferences(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("com.example.soumodippaul.newapp", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        for (int i=0;i<10;i++){
            editor.putBoolean((i+1)+"",false);
        }
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
